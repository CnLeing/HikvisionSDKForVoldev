# Hikvision Sadp SDK For Voldev

#### 介绍
海康搜索局域网设备SDK：基于私有搜索在线设备协议开发的SDK，动态库形式提供，适用于"硬件与客户端处于同一个子网"的网络环境，主要功能包括搜索在线设备、激活设备、修改设备网络参数、重置设备密码等。

#### 依赖环境
- 火山平台IDE

#### 移植进度
- SADP - 【完成】基于私有搜索在线设备协议开发的SDK，动态库形式提供，适用于"硬件与客户端处于同一个子网"的网络环境，主要功能包括搜索在线设备、激活设备、修改设备网络参数、重置设备密码等
- HCNet - 【基本完成】设备网络SDK是基于设备私有网络通信协议开发的，为海康威视各类硬件产品服务的配套模块，用于远程访问和控制设备的软件二次开发

- MediaPlayControl - 【基本完成】播放库SDK是我司网络硬盘录像机、视频服务器、IP设备等产品播放相关的二次开发包，适用于各系列编码产品录像文件和数据流的解码与播放。

#### 安装SDK
1.  前往 [码云代码托管](https://gitee.com/CnLeing/HikvisionSadpSDKForVoldev)地址 下载最新代码。

